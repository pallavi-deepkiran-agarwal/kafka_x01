package com.bitbucket.deepkiran.kafka.tutorial1;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerDemoKeys {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        final Logger logger = LoggerFactory.getLogger(ProducerDemoKeys.class);

        String bootstrap_server = "127.0.0.1:9092";


        //create producer properties
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap_server);

        /*this will let kafka know what
        type of message we are loading and
        how to serialize it*/
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        //create producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        //create producer record
        for (int i= 0; i<=10; i++) {
            String topic = "pallavi_topic";
            String message = "Hello Pallavi Agarwal with id_"+ Integer.toString(i);
            String key = "id_" + Integer.toString(i);
            ProducerRecord<String, String> record =
                    new ProducerRecord<String, String>(topic, key, message);

            //send data
            logger.info("key: "+ key); //log key
            producer.send(record, new Callback() {
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (e == null) {
                        logger.info("reciever new metadata \n" +
                                "Topic:" + recordMetadata.topic() + "\n" +
                                "offset:" + recordMetadata.offset() + "\n" +
                                "Partition:" + recordMetadata.partition() + "\n" +
                                "Timestamp:" + recordMetadata.timestamp());
                    } else {
                        logger.error("Error while producing", e);
                    }
                }
            }).get(); // block the .send to make it synchronous
        }
        //flush data
        producer.flush();
        //flush and close producer
        producer.close();
    }
}
