package com.bitbucket.deepkiran.kafka.tutorial1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoWithThread {
    public static void main(String[] args) {
        new ConsumerDemoWithThread().run();
    }

    public ConsumerDemoWithThread(){

    }

    public void run(){
        Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThread.class.getName());
        String bootstrap_server = "127.0.0.1:9092";
        String group = "my-sixth-application";
        String topic = "pallavi_topic";

        //latch for dealing with multiple threads
        CountDownLatch latch = new CountDownLatch(1);

        //create consumer runnable
        logger.info("creating consumer thread");
        //runnable here is the consumer thread
        Runnable myConsumerRunnable = new ConsumerRunnable(bootstrap_server, group, topic, latch);

        //start the thread
        Thread myThread = new Thread(myConsumerRunnable);
        myThread.start();

        //add a shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Caught shutdown hook");
            ((ConsumerRunnable)myConsumerRunnable).shutDown();
            try {
                latch.await();
            } catch (InterruptedException e) {
                logger.error("Application got interrupted ",e);
            }finally {
                logger.info("Application is closing");
            }

            logger.info("Application has exited");
        }
        ));
    }

    public class ConsumerRunnable implements Runnable{

        CountDownLatch latch;
        private Logger logger = LoggerFactory.getLogger(ConsumerRunnable.class.getName());
        private KafkaConsumer<String, String> consumer;

        public ConsumerRunnable(String bootstrap_server,
                              String group,
                              String topic,
                              CountDownLatch latch)
        {
            this.latch = latch;

            Properties properties = new Properties();
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap_server);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, group);

            consumer = new KafkaConsumer<String, String>(properties);
            consumer.subscribe(Collections.singleton(topic)); //singleton means you are subscribing to only one topic
        }

        @Override
        public void run() {
            try {
                while (true) {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, String> record : records) {
                        logger.info("key: " + record.key() + " Value: " + record.value());
                        logger.info("Partition: " + record.partition() + " Offset: " + record.offset());
                    }
                }
            }catch (WakeupException e){
                logger.info("Recieved shutdown event");
            }finally {
                consumer.close();
                latch.countDown();
            }


        }

        public void shutDown(){
            //this wakeup method is to interrupt consumer.poll()
            //this wil throw exception WakeupException
            consumer.wakeup();
        }
    }
}
